﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TopicClassification
{
    public partial class FilesSelect : Form
    {
        private string[] _selectedFiles;

        public FilesSelect()
        {
            InitializeComponent();
        }

        private void openFileDialog_Click(object sender, EventArgs e)
        {
            if(fileDialog.ShowDialog() == DialogResult.OK)
            {
                var files = fileDialog.FileNames;
                _selectedFiles = files;

                fileListView.Items.Clear();
                fileListView.Items.AddRange(files.Select(x => new ListViewItem
                {
                    Text = Path.GetFileName(x)
                }).ToArray());
            }
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            if(_selectedFiles != null && _selectedFiles.Length > 0)
            {
                var classificationForm = new Classification(_selectedFiles);
                this.Hide();
                classificationForm.ShowDialog();
                this.Close();
            }
        }
    }
}
