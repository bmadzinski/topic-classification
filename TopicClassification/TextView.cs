﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TopicClassification
{
    public partial class TextView : Form
    {
        public TextView()
        {
            InitializeComponent();
        }

        public string[] CurrentLines
        {
            get => richTextBox1.Lines;
            set => richTextBox1.Lines = value;
        }

        public int CurrentLine
        {
            get => richTextBox1.GetLineFromCharIndex(richTextBox1.SelectionStart);
            set
            {
                var start = richTextBox1.GetFirstCharIndexFromLine(value);
                var length = richTextBox1.Lines[value].Length;
                richTextBox1.Select(start, length);
            }
        }
    }
}
