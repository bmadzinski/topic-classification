﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TopicClassification
{
    public enum Classes
    {
        Title,
        Content,
        DateMade,
        Between,
        Address
    }

    public partial class Classification : Form
    {
        private string[] _files;
        private int _currentFileIndex;

        private string _progressLabel => string.Format("{0} z {1}", _currentFileIndex + 1, _files.Length);
        private string _currentFile => _files[_currentFileIndex];
        private int _classNumber => 5;

        private List<string> _currentLines = new List<string>();
        private List<CheckBox> _selectAllCheckboxes = new List<CheckBox>();

        public Classification(string[] files)
        {
            _files = files;
            _currentFileIndex = 0;

            InitializeComponent();

            _selectAllCheckboxes.Add(class1cb);
            class1cb.Tag = 0;
            for (int i = 1; i < _classNumber; i++)
            {
                var cb = new CheckBox();
                cb.Tag = i;
                cb.Bounds = class1cb.Bounds;
                cb.Location = new Point(_selectAllCheckboxes[i - 1].Location.X + _selectAllCheckboxes[i - 1].Bounds.Width, _selectAllCheckboxes[i - 1].Location.Y);
                _selectAllCheckboxes.Add(cb);
                this.Controls.Add(cb);
            }

            foreach (var ch in _selectAllCheckboxes)
            {
                ch.Click += selectAllClick;
            }

            foreach (var e in Enum.GetValues(typeof(Classes)))
            {
                fileGridView.Columns.Add(new DataGridViewCheckBoxColumn()
                {
                    Selected = false,
                    Name = e.ToString(),
                    AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader,
                });
            }

            fileGridView.AllowUserToAddRows = false;
            fileGridView.AllowUserToDeleteRows = false;
            fileGridView.AllowUserToOrderColumns = false;

            //for(int i = 0; i < _classNumber; i++)
            //{
            //    fileGridView.Columns.Add(new DataGridViewCheckBoxColumn()
            //    {
            //        Selected = false,
            //        Name = i.ToString(),
            //        AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader,
            //    });
            //}

            fileGridView.CellValueChanged += FileGridViewOnCellValueChanged;
            fileGridView.CellMouseUp += FileGridViewOnCellMouseUp;
            fileGridView.KeyUp += FileGridViewOnKeyUp;
        }

        private void FileGridViewOnKeyUp(object sender, KeyEventArgs keyEventArgs)
        {
            if (fileGridView.SelectedCells.Count < 1)
            {
                return;
            }

            var currentCell = fileGridView.SelectedCells[0];
            var currentRow = fileGridView.Rows[currentCell.RowIndex];
            int columnIndex;

            switch (keyEventArgs.KeyCode)
            {
                case Keys.D1:
                    columnIndex = 1;
                    break;
                case Keys.D2:
                    columnIndex = 2;
                    break;
                case Keys.D3:
                    columnIndex = 3;
                    break;
                case Keys.D4:
                    columnIndex = 4;
                    break;
                case Keys.D5:
                    columnIndex = 5;
                    break;
                default:
                    return;
            }

            var cell = currentRow.Cells[columnIndex] as DataGridViewCheckBoxCell;
            if (cell == null)
            {
                return;
            }

            cell.Value = true;

            if (fileGridView.Rows.Count != currentCell.RowIndex + 1)
            {
                currentCell.Selected = false;
                fileGridView.CurrentCell = fileGridView.Rows[currentCell.RowIndex + 1].Cells[currentCell.ColumnIndex];
                //fileGridView.Rows[currentCell.RowIndex + 1].Cells[currentCell.ColumnIndex].Selected = true;
            }
        }

        private void FileGridViewOnCellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex > 0 && e.RowIndex != -1)
            {
                fileGridView.EndEdit();
            }
        }

        private void FileGridViewOnCellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex > 0 && e.RowIndex != -1)
            {
                fileGridView.CellValueChanged -= FileGridViewOnCellValueChanged;
                for (var i = 1; i < fileGridView.Rows[e.RowIndex].Cells.Count; i++)
                {
                    var cell = fileGridView.Rows[e.RowIndex].Cells[i] as DataGridViewCheckBoxCell;
                    if (cell == null)
                    {
                        continue;
                    }

                    if (i == e.ColumnIndex)
                    {
                        if ((bool?)cell.Value == true)
                        {
                            cell.Style.BackColor = Color.Green;
                        }
                        else
                        {
                            cell.Style.BackColor = Color.White;
                        }
                        continue;
                    }

                    cell.Value = false;
                    cell.Style.BackColor = Color.White;
                }
                fileGridView.CellValueChanged += FileGridViewOnCellValueChanged;
            }
        }

        private void SetCellValue(int rowIndex, int columnIndex, bool value)
        {

        }

        private void selectAllClick(object sender, EventArgs e)
        {
            var cb = sender as CheckBox;

            for (int i = 0; i < fileGridView.Rows.Count; i++)
            {
                var row = fileGridView.Rows[i];

                row.Cells[((int)cb.Tag) + 1].Value = cb.Checked;
            }

        }

        private void Classification_Load(object sender, EventArgs e)
        {
            UpdateData();
        }

        private void UpdateData()
        {
            progressLabel.Text = _progressLabel;
            fileGridView.Rows.Clear();
            _currentLines.Clear();

            var rows = getFileRows(_currentFile);
            foreach (var rowText in rows)
            {
                _currentLines.Add(rowText);

                if (!string.IsNullOrEmpty(rowText))
                    fileGridView.Rows.Add(rowText);
            }
            if (_currentFileIndex == _files.Length - 1)
                nextButton.Text = "Zakończ";
        }

        private List<string> getFileRows(string file)
        {
            var rows = new List<string>();
            using (var stream = File.OpenRead(file))
            {
                using (var sr = new StreamReader(stream))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        rows.Add(line);
                    }

                }
            }
            return rows;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            var rows = new List<RowData>();

            var viewRowIndex = 0;
            for (int rowIndex = 0; rowIndex < _currentLines.Count; rowIndex++)
            {
                if (string.IsNullOrEmpty(_currentLines[rowIndex]))
                {
                    rows.Add(new RowData()
                    {
                        Text = _currentLines[rowIndex],
                        Class = null
                    });

                }
                else
                {
                    var rowCells = fileGridView.Rows[viewRowIndex].Cells;

                    int? selectedIndex = null;
                    for (int classIndex = 0; classIndex < _classNumber; classIndex++)
                    {
                        if ((bool?)rowCells[classIndex + 1].Value == true)
                        {
                            if (selectedIndex.HasValue)
                            {
                                MessageBox.Show($"Wiele zaznaczeń na linii {viewRowIndex}.", "Błąd", MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                                return;
                            }

                            selectedIndex = classIndex;
                            //break;
                        }
                    }

                    rows.Add(new RowData()
                    {
                        Text = _currentLines[rowIndex],
                        Class = selectedIndex
                    });
                    viewRowIndex++;
                }
            }

            var direcotry = Path.GetDirectoryName(_currentFile);
            var filename = Path.GetFileNameWithoutExtension(_currentFile);
            var jsonFilepath = Path.Combine(direcotry, $"{filename}.json");

            using (var stream = File.Open(jsonFilepath, FileMode.Create))
            {
                using (var sw = new StreamWriter(stream))
                {
                    using (var jtw = new JsonTextWriter(sw))
                    {
                        jtw.Formatting = Formatting.Indented;
                        var serizalizer = new JsonSerializer();
                        serizalizer.Serialize(jtw, rows);
                    }

                }
            }
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            if (++_currentFileIndex < _files.Length)
                UpdateData();
            else
                this.Close();
        }
    }

    class RowData
    {
        public string Text { get; set; }
        public int? Class { get; set; }
    }
}
